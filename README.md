This project contains two main scripts :

1. viewmodels.py
This script authomatise the transformation from a file produced by Mace4. 
Such a file is transformed into xml (using interpformat), then the interpretations are transformend into dot graphs (via the dot language), 
and finaally trasnformed into pdf.

2. proofs.py 
This is, essentially, gvizify. All the transformations (->xml->dot->pdf) are batched, so to the ease of students.

Usage :

./viewmodels.py fileProducedByMace4
./viewproofs.py fileProducedByProver9

Dependencies: 

From Prover9-Mace4: interpformat, gvizify, prooftrans
(this files are also in the subfolder fromP9M4)

From Graphviz: dot 

For opening pdf files, we use
On Linux:
evince 
On Mac:
Apercu (should we add Preview ?)